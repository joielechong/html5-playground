 ;
 (function($) {
     $.addEventListener('DOMContentLoaded', function() {
         var body = $.getElementsByTagName('body')[0],
             obj = $.createElement('div');

         obj.style.width = 100 + 'px';
         obj.style.height = 100 + 'px';
         obj.style.backgroundColor = '#F00';

         obj.style.position = 'absolute';
         obj.style.webkitTransform = 'translateX(' + 200 + 'px)';
         obj.style.mozTransform = 'translateX(' + 200 + 'px)';
         obj.style.mozTransform = 'translateX(' + 200 + 'px)';
         obj.style.msTransform = 'translateX(' + 200 + 'px)';
         obj.style.transform = 'translateX(' + 200 + 'px)';

         body.appendChild(obj);

     }, false);
 }(document));